<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CaptchaValidationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

 
 
// Route::get('form', [CaptchaValidationController::class, 'index']);
// Route::get('form/{id_dokumen}', [CaptchaValidationController::class, 'index']);
// Route::post('/captcha-validation/{id_dokumen}', [CaptchaValidationController::class, 'getEsign']);
// Route::post('captcha-validation', [CaptchaValidationController::class, 'capthcaFormValidate']);
Route::match(['get', 'post'],'captcha-validation/{id_dokumen}', [CaptchaValidationController::class, 'capthcaFormValidate']);
Route::get('reload-captcha', [CaptchaValidationController::class, 'reloadCaptcha']);
Route::get('{id_dokumen}', [CaptchaValidationController::class, 'getEsign']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
