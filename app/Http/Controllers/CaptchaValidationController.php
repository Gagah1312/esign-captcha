<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Validator;

class CaptchaValidationController extends Controller
{
    // public function index($id_dokumen)
    public function index(Request $request)
    {

        // $getiddokumen = Http::get('http://127.0.0.1:8008/api/esign/' . $id_dokumen);
        // $responseBody = ($getiddokumen->json());

        // return view('validation', compact('responseBody'));

        // $getdata = DataEsign::where('id_dokumen', '=', $new_esign->id_dokumen)->first();
        // $response = Http::get('http://127.0.0.1:8008/api/esign/' . $id_dokumen);

        // $responseBody = ($response->json());
        // // dd($responseBody);

        $request->validate([
            // 'name' => 'required',
            // 'email' => 'required|email',
            // 'message' => 'required',
            // 'captcha' => 'required|captcha'
            'g-recaptcha-response' => 'required|captcha',
            // 'g-recaptcha-response' => 'recaptcha',
        ]);

        // return view('validation', compact('responseBody'));
        return view('validation');
    }

    public function capthcaFormValidate(Request $request, $id_dokumen)
    {
        // $request->validate([
        //     // 'name' => 'required',
        //     // 'email' => 'required|email',
        //     // 'message' => 'required',
        //     // 'captcha' => 'required|captcha'
        //     'g-recaptcha-response' => 'required|captcha',
        //     // 'g-recaptcha-response' => 'recaptcha',
        // ]);

        $response = Http::get('http://127.0.0.1:8008/api/esign/' . $id_dokumen);

        $responseBody = ($response->json());
        // dd($responseBody);

        return view('data.data-esign', compact('responseBody'));
    }

    public function reloadCaptcha()
    {
        return response()->json(['captcha' => captcha_img()]);
    }

    public function getEsign(Request $request, $id_dokumen)
    {
        // $request->validate([
        //     // 'name' => 'required',
        //     // 'email' => 'required|email',
        //     // 'message' => 'required',
        //     // 'captcha' => 'required|captcha'
        //     'g-recaptcha-response' => 'required|captcha',
        //     // 'g-recaptcha-response' => 'recaptcha',
        // ]);

        // $validated = $request->validate([
        //     'g-recaptcha-response' => 'required|captcha',
        // ]);

        $disabled = false;

        $response = Http::get('http://127.0.0.1:8008/api/esign/' . $id_dokumen);

        $responseBody = ($response->json());
        // dd($responseBody);
        // dd(count($responseBody));

        return view('data.data-esign', compact('responseBody', 'disabled'));
    }
}
