@extends('app')
@section('content')

    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    {{-- <h1><strong>Bootstrap</strong> Login Form</h1>
                    <div class="description">
                        <p>
                            This is a free responsive login form made with Bootstrap.
                            Download it <a href="https://github.com/AZMIND" target="_blank"><strong>here</strong></a>,
                            customize and use it as you like!
                        </p> --}}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <img src="assets/img/logo-rsdi.png" style="height: 50; width: auto">
                    </div>
                    <div class="form-top-right">
                        <p></p>
                        <i class="fa fa-lock"> VALIDASI E-SIGN </i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form action="#">
                        @csrf
                        <center>
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div id="recapchaWidget" class="g-recaptcha"
                                        data-sitekey="6LdGnXojAAAAAOICd3sDhilyUPYORRMC1q3RH9Kt"
                                        data-callback="correctCaptcha">

                                    </div>
                                </div>
                            </div>&nbsp&nbsp
                        </center>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-4 text-left">
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 text-left">
                                <button type="button" id="submit" data-toggle="modal" data-target="#modalMd"
                                    class="btn btn-primary">
                                    <b>Cek</b></button>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 text-left">
                            </div>
                        </div>
                </div>
                </form>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 social-login" style="color: rgb(255, 255, 255);">
                <strong>Copyright IPTI &copy; <?php echo date('Y'); ?> e-Sign Team.</strong> All rights reserved.
            </div>
        </div>
    </div>

    {{-- START MODAL DATA --}}
    <div class="modal fade" id="modalMd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-body">
                <div class="col-sm-12 col-sm-offset-0 form-box">
                    <div class="form-top-modal">
                        <div class="form-top-left">
                            <img src="assets/img/logo-rsdi.png" style="height: 50; width: auto">
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-lock"></i>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true" style="color: azure">&times;</span></button>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <?php
                        function getaddress($lat, $lng)
                        {
                            $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($lng) . '&key=AIzaSyCSNW7Pt4PQZ7qxeT6rrTAQoBqpcw51KBE';
                            $json = @file_get_contents($url);
                            $data = json_decode($json);
                            if ($data != null) {
                                return $data->results[0]->address_components[5]->long_name;
                            } else {
                                return false;
                            }
                        }
                        ?>

                        @if ($responseBody['meta']['description'] === 'VALID')
                            <div class="logo">
                                <center><img src="{{ url('assets/img/valid.png') }}"></center>
                            </div>
                            <div class="row" style="margin: 0px 75px 0px;">
                                <hr>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <b>Status</b>
                                    <br>
                                    <b style="color: green">
                                        <?php echo $responseBody['meta']['description']; ?>
                                    </b>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 mb-2">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 mb-2">
                                    <b>Nama Dokumen</b>
                                    <br>
                                    <?php echo $responseBody['response'][0]['dokumen']; ?>
                                </div>
                            </div>
                            <div class="row" style="margin: 15px 75px 0px;">
                                <b style="color:blue">Info Penandatanganan</b>
                                <hr>
                                @for ($i = 0; $i < count($responseBody['response']); $i++)
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        @if (count($responseBody['response']) > 1)
                                            <b>Penandatangan <?php echo $i + 1; ?> </b>
                                        @else
                                            <b>Penandatangan</b>
                                        @endif
                                        <br>
                                        <?php echo $responseBody['response'][$i]['pemberi_tta']; ?>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <b>Lokasi</b>
                                        <br>
                                        <?php
                                        $lokasi = $responseBody['response'][$i]['lokasi'];
                                        // memisahkan string menjadi array
                                        $data = explode(',', $lokasi);

                                        $lat = $data[0];
                                        $lng = $data[1];
                                        $address = getaddress($lat, $lng);
                                        if ($address) {
                                            echo $address;
                                        } else {
                                            echo '-';
                                        }
                                        ?>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <b>Tanggal</b>
                                        <br> &nbsp;
                                        <span class="fa fa-calendar-check-o"><?php echo ' ' . $responseBody['response'][$i]['tgl_tta']; ?></span>
                                    </div>
                                @endfor
                            </div>
                            <div class="row" style="margin: 15px 75px 100px;">
                                <b style="color:blue">Info Tambahan</b>
                                <hr>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <b>Waktu Pindai</b>
                                    <br>
                                    <?php
                                    date_default_timezone_set('Asia/Jakarta');
                                    ?>
                                    <span class="fa fa-calendar"><?php echo ' ' . date('Y-d-m') . '   '; ?></span>
                                    <span class="fa fa-clock-o"><?php echo ' ' . date('H:i:s'); ?></span>
                                </div>
                            </div>
                            <div class="form-bottom-info">
                                <div class="row">
                                    <p></p>
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <b style="color: rgb(56, 56, 56);">
                                            Dokumen ini telah ditandatangani secara elektronik
                                            menggunakan e-Sign milik RSUD dr. Iskak Tulungagung</b>
                                    </div>
                                </div>
                            </div>
                        @elseif($responseBody['response']['status'] === 'TIDAK VALID')
                            <div class="logo">
                                <center><img src="{{ url('assets/img/invalid.png') }}"></center>
                            </div>
                            <div class="row" style="margin: 0px 100px 175px;">
                                <hr>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <center>
                                        <div class="form-group">
                                            <b>Status</b>
                                            <br>
                                            <b style="color: red">
                                                <?php echo $responseBody['response']['status']; ?>
                                            </b>
                                        </div>
                                    </center>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- END MODAL DATA --}}

    <script>
        $(document).on('ajaxComplete ready', function() {
            $('.modalMd').off('click').on('click', function() {
                $('#modalMdContent').load($(this).attr('value'));
                $('#modalMdTitle').html($(this).attr('title'));
            });
        });
    </script>


    <script>
        document.addEventListener('contextmenu', event => event.preventDefault());
        document.onkeydown = function(e) {
            if (event.keyCode == 123) {
                return false;
            }
            if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.keyCode == 'S'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
                return false;
            }
        }
    </script>


    <script>
        const formInput = document.querySelector(".form-group");
        const formButton = document.querySelector(".btn-primary");
        formButton.disabled = true;
        var correctCaptcha = function(response) {

            if (response.length != 0) {
                formButton.disabled = false;
            }
        };

        // var res = grecaptcha.getResponse(widId);
        // the default state is 'disabled'
        // formButton.disabled = true;

        // alternative is to use "change" - explained below
        formInput.addEventListener("keyup", buttonState);

        function buttonState() {
            if (document.querySelector(".form-group").value === "") {
                formButton.disabled = true; // return disabled as true whenever the input field is empty
            } else {
                formButton.disabled = false; // enable the button once the input field has content
            }
        }

        // just verifying that the button has been clicked
        formButton.addEventListener("click", () => {
            console.log("You entered:", document.querySelector(".form-group").value);
        });
    </script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>



    {{-- <script>
        const formInput = document.querySelector(".form-group");
        const formButton = document.querySelector(".btn-primary");
        formButton.disabled = true;
        var correctCaptcha = function(response) {

            if (response.length != 0) {
                formButton.disabled = false;
            }
        }
        //   return true;
            // formButton.disabled = true;
            // if (document.querySelector(".form-group").res == "" || res == undefined || res.length == 0) {
            //     formButton.disabled = true; // return disabled as true whenever the input field is empty
            // } else {
            //     formButton.disabled = false; // enable the button once the input field has content
            // }
        //  }


    </script>  --}}
